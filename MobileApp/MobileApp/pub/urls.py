from django.conf.urls import patterns, url

from MobileApp.pub import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^order', views.orders, name='orders'),
    url(r'^user_orders', views.user_orders, name='user_orders'),
    url(r'^restaurants/(?P<id>[0-9]+)$', views.restaurant, name='restaurant'),
    url(r'^restaurants', views.restaurants, name='restaurants'),
    
)
