from django.db import models
from django.contrib.auth.models import User
from decimal import Decimal

class Zamowienie(models.Model):
    jakie_jedzenie = models.CharField(max_length=70)
    data_zlozenia = models.DateTimeField('Data zlozenia')
    data_odbioru = models.DateTimeField('Data odbioru')
    dodatkowe_info = models.TextField()
    def __str__(self):
        return self.jakie_jedzenie

class Customer(models.Model):
    name = models.CharField(max_length = 100)
    def __str__(self):
        return self.name	
	
class Restaurant(models.Model):
    id = models.AutoField(primary_key=True,default=0)
    name = models.CharField(max_length=70,default = '')
    adress = models.CharField(max_length=70, default='')
    phone = models.CharField(max_length=70, default='')
    info = models.TextField(default='')
    def __str__(self):
        return self.name
	
class MenuEntry(models.Model):
    restaurant = models.ForeignKey(Restaurant)
    name = models.CharField(max_length = 100)
    price = models.DecimalField(max_digits=16, decimal_places=2,
                                default=Decimal(0.00))
    description = models.TextField(default='')
    def __str__(self):
        return self.name	
	
class Order(models.Model):
    customer = models.ForeignKey(Customer)
    restaurant = models.ForeignKey(Restaurant)
    menu_entry = models.ForeignKey(MenuEntry)
    order_date = models.DateTimeField('Data zlozenia')
    ready_date = models.DateTimeField('Data odbioru')
    def __str__(self):
        return self.menu_entry.name	
	
class ExtUser(models.Model):
    user = models.OneToOneField(User)
    is_customer = models.BooleanField()
    restaurant = models.OneToOneField(Restaurant)

	
	
