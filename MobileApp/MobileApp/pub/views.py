# -*- coding: utf-8 -*-
# Some standard Django stuff
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import Context, RequestContext, loader
from django import template
from django.contrib.auth.models import User
from MobileApp.pub.models import Zamowienie, Restaurant, MenuEntry, ExtUser
from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login
from decimal import Decimal

def login_user(request):
    state = "Please log in below..."
    username = password = ''
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                state = "You're successfully logged in!"
                return restaurants(request)
            else:
                state = "Your account is not active, please contact the site admin."
        else:
            state = "Your username and/or password were incorrect."

        if 'cnt' in request.session:
            state = "Powracasz na ta strone po raz" + str(request.session['cnt'] +1) + "\n" + state
            request.session['cnt'] = request.session['cnt'] + 1
        else:
            request.session['cnt'] = 1
            state = "Odwiedzasz ta strone 1 raz\n" + state

    return render_to_response('auth.html',{'state':state, 'username': username})

 
def index(request):
    '''Render the index page'''
    t = loader.get_template('index.html')
    c = Context( { }) # normally your page data would go here
    return HttpResponse(t.render(c))

def orders(request):
	return HttpResponse("Zamówienia.")

def user_orders(request):
    orders_list = Zamowienie.objects.all()
    template = loader.get_template('user_orders.html')
    context = RequestContext(request, {'orders_list' : orders_list})
    return HttpResponse(template.render(context))

def restaurants(request):
    restaurants_list = Restaurant.objects.all()
    template = loader.get_template('restaurants.html')
    context = RequestContext(request, {'restaurants_list' : restaurants_list})
    return HttpResponse(template.render(context))

def restaurant_page(request, id):
    restaurant= Restaurant.objects.get(id = id)    
    template = loader.get_template('restaurant.html')
    context = RequestContext(request, {'restaurant' : restaurant})
    menu = MenuEntry.objects.filter(restaurant = id)          
    template = loader.get_template('restaurant.html')
    context = RequestContext(request, {'restaurant' : restaurant, 'menu' : menu})
    return HttpResponse(template.render(context))

def restaurant_profile(request, id):
    restaurant= Restaurant.objects.get(id = id)    
    template = loader.get_template('restaurant_profiles.html')
    context = RequestContext(request, {'restaurant' : restaurant})
    menu = MenuEntry.objects.filter(restaurant = id)          
    template = loader.get_template('restaurant.html')
    context = RequestContext(request, {'restaurant' : restaurant, 'menu' : menu})
    return HttpResponse(template.render(context))

def add_menu_entry(request):
    if request.POST:
        name = request.POST.get('name')
        cena = request.POST.get('price')
        price = Decimal(cena)
        price.quantize(Decimal(0.00))
        description = request.POST.get('description')
        restaurant_id = request.POST.get('Rest_id')
        restaurant = Restaurant.objects.get(id = restaurant_id)
        MenuEntry.objects.create(name = name, restaurant= restaurant,
                                 price = price, description = description)        
        return restaurant_page(request, restaurant_id)

def register(request):
    t = loader.get_template('register.html')
    c = Context( { }) 
    return HttpResponse(t.render(c)) 

def register_restaurant(request, username):
    t = loader.get_template('register_restaurant.html')
    c = Context( { }) 
    return HttpResponse(t.render(c))      

def add_user(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        is_customer = request.POST.get('is_cust')
        print(is_customer)   
        User.objects.create_user(username, email, password)  
        user = User.objects.get(username = username)
        print(user.username)
        if is_customer is not None:
            ExtUser.objects.create(user = user, is_customer = is_customer)
            return login_user(request)
        else: 
            ExtUser.objects.create(user = user, is_customer = is_customer)
            return register_restaurant(request, username)

def choose_side(request):
    t = loader.get_template('choose_side.html')
    c = Context( { }) 
    return HttpResponse(t.render(c))   

def reg_user(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        email = request.POST.get('email')
        is_customer = request.POST.get('is_cust')
        print(is_customer)   
        User.objects.create_user(username, email, password)  
        user = User.objects.get(username = username)
        restaurant = Restaurant.objects.get(id = 0)
        print(user.username)
        ExtUser.objects.create(user = user, is_customer = True, restaurant = restaurant)
        return login_user(request)

def help(request):
    t = loader.get_template('help.html')
    c = Context( { }) 
    return HttpResponse(t.render(c)) 
