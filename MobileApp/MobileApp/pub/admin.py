from django.contrib import admin
from MobileApp.pub.models import Zamowienie, Restaurant, Customer, Order

class ZamowienieAdmin(admin.ModelAdmin):
    fields = ['jakie_jedzenie', 'data_zlozenia',
              'data_odbioru', 'dodatkowe_info']
    list_display = ('jakie_jedzenie', 'data_zlozenia',
              'data_odbioru')
  
admin.site.register(Zamowienie,ZamowienieAdmin)

class AdminsRestaurants(admin.ModelAdmin):
    fields = ['name', 'adress', 'phone', 'info']

    list_display = ('name', 'adress', 'phone', 'info')


admin.site.register(Restaurant,AdminsRestaurants)

admin.site.register(Customer)
admin.site.register(Order)

# Register your models here.
