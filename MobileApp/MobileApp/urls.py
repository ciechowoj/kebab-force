from django.conf.urls import patterns, include, url
from django.contrib import admin
from MobileApp.pub import views

urlpatterns = patterns('',
	url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index, name='index'),
    url(r'^order', views.orders, name='orders'),
    url(r'^user_orders', views.user_orders, name='user_orders'),
    url(r'^restaurants/([0-9]+)$', views.restaurant_page, name='restaurant_page'),
    url(r'^restaurants', views.restaurants, name='restaurants'), 
    url(r'^login', views.login_user, name='login'), 
	url(r'^add_menu_entry', views.add_menu_entry, name='add_menu_entry'),
	url(r'^reg', views.register, name='register'), 
	url(r'^add_user', views.add_user, name='add_user'),
    url(r'^choose_side', views.choose_side, name='choose_side'),
    url(r'^reg_user', views.reg_user, name='reg_user'),
    url(r'^help', views.help, name='help'), 

)
