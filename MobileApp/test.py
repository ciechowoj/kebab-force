s-from django.test import TestCase
from django.contrib.auth.models import User

class UsersAdding(TestCase):
    def test_adding(self):
    	"""Users adding correctly"""
    	User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
    	user = User.objects.get(username = 'john')
    	self.assertEqual(user.email, 'lennon@thebeatles.com')

